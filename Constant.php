<?php

require_once("includes.php");

/**
 * The two constants in Boolean algebra are the characters 1 and 0.
 */
class Constant extends SimpleFormula implements SimplePrint
{
    private $myValue; // 1 or 0

    public function __construct($c)
    {
        if ($c != "1" && $c != "0")
            throw new SyntaxException("Constant needs to be 1 or 0");
        else
            $this->myValue = $c;
    }

    /**
     * Uniformly subtstitutes every occurrence of $a in me with Formula $f.
     * Since I have no internal Atom, this method does nothing.
     * @param  Formula $f         Formula that is going to sub in for
     *                            occurrences of $a
     * @param  Atom    $a         Atom that gets swapped out if in me.
     * @return Constant           It's just me.
     */
    public function uSub(Formula $f, Atom $a)
    {
        return $this;
    }

    /**
     * Answers the question: are you obtainable from me through uniform
     * substitution? with "not unless you are me"
     * @param  Formula $uSub [description]
     * @return [type]        [description]
     */
    public function uSubbable(Formula $uSub)
    {
        return $this->equals($uSub);
    }

    public function uSubbableConstrained(Formula $f, &$myMap)
    {
        return $this->equals($f);
    }

    public function toString()
    {
        return $this->myValue;
    }
}

?>