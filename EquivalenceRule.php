<?php

require_once("includes.php");

/**
 * An EquivalenceRule allows the interderivation of any pair of Formulae that
 * can be obtained through uniform substitution with its Equivalence It has a
 * string name (could be e.g. DeMorgans) and an Equivalence.
 */
class EquivalenceRule
{
    private $name;
    private $equiv;

    public function getName()
    {
        return $this->name;
    }

    public function getEquiv()
    {
        return $this->equiv;
    }

    public function getLeft()
    {
        return $this->equiv->getLeft();
    }

    public function getRight()
    {
        return $this->equiv->getRight();
    }

    public function __construct($name, $left, $right) // should all be strings.
    {
        $this->equiv = new Equivalence($left, $right);
        // Equivalence class will take care of validation

        // We do have to validate the name though...
        if (is_string($name))
            $this->name = $name;
        else
            throw new InvalidArgumentException("name must be a string");
    }

    public function toString()
    {
        return $this->name . " " . $this->equiv->toString();
    }

    public function toShortString()
    {
        return $this->name;
    }

    /**
     * Here we dig into some of the deeper algoritmic basis of BoolEdit. This
     * is what EquivalenceRules are for.
     * @param  Formula $firstLine  A line in a proof
     * @param  Formula $secondLine Another line in a proof
     * @return boolean             Boolean that is true just in case the
     *                             firstline and secondline are interderivable
     *                             using this rule.
     */
    public function licensedTransitionWhole(Formula $firstLine, Formula $secondLine)
    {
        $output = false;
        $subMap = array();

        if ($this->getLeft()->uSubbable($firstLine) &&
            $this->getRight()->uSubbable($secondLine))
        {
            $subMap = $this->getLeft()->getUSubMap($firstLine);
            $output = $this->getRight()->uSubbableConstrained($secondLine, $subMap);
        }
        else if ($this->getRight()->uSubbable($firstLine) &&
                 $this->getLeft()->uSubbable($secondLine))
        {
            $subMap = $this->getLeft()->getUSubMap($secondLine);
            $output = $this->getRight()->uSubbableConstrained($firstLine, $subMap);
        }

        return $output;
    }

    /**
     * A more sophisticated extension of licensedTransitionWhole. Permits the interderivation
     * of its two arguments by licensedTramsitionWholes to their analagous subformulae
     * (but only one pair at a time). So for example, you could use the double negation rule
     * to derive p+q*r from p+(q+r)''.
     * @param  Formula $f1 First Formula
     * @param  Formula $f2 Second Formula
     * @return [type]      Boolean that is true just in case the two formulae
     *                     are interderivable if we apply this rule to one
     *                     subformula.
     */
    public function licensedTransitionPart(Formula $f1, Formula $f2)
    {
        // Starts by checking whether the current analagous pair represent a
        // transition that this rule licenses.
        $output = $this->licensedTransitionWhole($f1, $f2);

        // This triggers if the current pair of Formulae under consideration
        // don't represent a licensed transition for this rule
        if ( !$output &&
             !($f1 instanceof SimpleFormula) &&  // Recursive basis
             !($f2 instanceof SimpleFormula) )   // ... when you reach the leaf
        {
            $idx = $this->idxOfExactlyOneArgumentDifference($f1, $f2);

            // Recursive step: if you aren't at a leaf and exactly one of F1 and F2s
            // arguments is different, drill down further.
            if ($idx >= 0)
                $output = $this->licensedTransitionPart($f1->getFormulae()[$idx],
                                                        $f2->getFormulae()[$idx]);
        }
        return $output;
    }

    /**
     * This function does two things at once.
     * First, it checks whether two formulae
     * are exactly alike except for a change to one of their subformulae. So for
     * example P * Q and P * (Q'' * R) fit this criterion, but P * Q and R * S
     * do not.
     * Second, it returns the index of those different formulae (it will be
     * identical) so that the algorithm can drill deeper in parallel into both
     * formulae.
     * @param  Formula $f1 First Formula
     * @param  Formula $f2 Second Formula
     * @return boolean     If f1 and f2 have exactly one argument difference,
     *                     this will be the index of that argument. Otherwise
     *                     it will be negative, indicating failure.
     */
    public function idxOfExactlyOneArgumentDifference(Formula $f1, Formula $f2)
    {
        $output = -1;
        // the default, will return if next part is unsuccessful

        if (!is_null($f1) &&!is_null($f2))
        {
            if ($f1 instanceof SimpleFormula && $f2 instanceof SimpleFormula)
                $output = -2; // does nothing, sorry for any confusion
            else if ($f1->getConnective() == ($f2->getConnective()))
            {
                $differenceCount = 0;
                $temp = -1;

                for ($i = 0; $i < $f1->getArity(); $i++)
                {
                    if (!$f1->getFormulae()[$i]->equals($f2->GetFormulae()[$i]))
                    {
                        $differenceCount++;
                        $temp = $i;
                    }
                }

                if ($differenceCount == 1)
                    $output = $temp;
            }
        }

        return $output;
    }
}

?>