<?php

require_once("includes.php");

/**
 * Static class that has a Singleton RulesSet that all the EquivalenceRules used
 * in COMP 1113 at BCIT.
 */
class BooleRules
{
    private static $myRules = null;

    private static function getRules()
    {
        if (BooleRules::$myRules == null)
        {
            BooleRules::$myRules = new RulesSet();

            // Define operator behaviour

            BooleRules::$myRules->addRule("P2a", "0 * 0 = 0");
            BooleRules::$myRules->addRule("P2b", "0 + 0 = 0");
            BooleRules::$myRules->addRule("P3a", "1 * 1 = 1");
            BooleRules::$myRules->addRule("P3b", "1 + 1 = 1");
            BooleRules::$myRules->addRule("P4a", "1 * 0 = 0");
            BooleRules::$myRules->addRule("P4b", "1 + 0 = 1");
            BooleRules::$myRules->addRule("P5a", "1' = 0");
            BooleRules::$myRules->addRule("P5b", "0' = 1");

            // Define algebraic laws

            BooleRules::$myRules->addRule("L6a", "x * y = y * x");
            BooleRules::$myRules->addRule("L6b", "x + y = y + x");
            BooleRules::$myRules->addRule("L7a", "x * (y * z) = (x * y) * z");
            BooleRules::$myRules->addRule("L7b", "x + (y + z) = (x + y) + z");
            BooleRules::$myRules->addRule("L8a", "x * (y + z) = x * y + x * z");
            BooleRules::$myRules->addRule("L8b", "x + (y * z) = x + y * x + z");

            // Define theorems

            BooleRules::$myRules->addRule("T9a", "x * 0 = 0");
            BooleRules::$myRules->addRule("T9b", "x + 0 = x");
            BooleRules::$myRules->addRule("T10a", "x * 1 = x");
            BooleRules::$myRules->addRule("T10b", "x + 1 = 1");
            BooleRules::$myRules->addRule("T11a", "x * x = x");
            BooleRules::$myRules->addRule("T11b", "x + x = x");
            BooleRules::$myRules->addRule("T12a", "x * x' = 0");
            BooleRules::$myRules->addRule("T12b", "x + x' = 1");
            BooleRules::$myRules->addRule("T13a", "x'' = x");
            BooleRules::$myRules->addRule("T13b", "x = x''");
            BooleRules::$myRules->addRule("T14a", "x + (x * y) = x");
            BooleRules::$myRules->addRule("T14b", "x * (x + y) = x");
            BooleRules::$myRules->addRule("T14c", "x * (x' + y) = x * y");
            BooleRules::$myRules->addRule("T14d", "x + x' * y = x + y");
            BooleRules::$myRules->addRule("T15a", "(x * y)' = x' + y'");
            BooleRules::$myRules->addRule("T15b", "(x + y)' = x' * y'");
        }
    }

    public static function getRule($name)
    {
        if ($name == "null") return null;

        BooleRules::getRules();
        return BooleRules::$myRules->getRule($name);
    }

    public static function toStringArray()
    {
        BooleRules::getRules();
        return BooleRules::$myRules->toStringArray();
    }
}

?>