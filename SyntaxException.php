<?php

/**
 * Thrown whenever an actual parameter violates the syntax of Boolen algebra.
 * The set of valid Boolean formulae is defined by me as the smallest set that
 * includes 0, 1, and [a-zA-Z] and is closed under conjunction, disjunction and
 * postfix negation.
 */
class SyntaxException extends Exception
{
}

?>