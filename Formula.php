<?php

require_once("includes.php");

/**
 * Formula objects follow the GoF Composite design pattern. A Formula can
 * be either a SimpleFormula (atom or constant), or a ComplexFormula built
 * up out of SimpleFormulae. This facilitates the core function of BoolEdit,
 * which is checking to see whether derivation rules have been correctly
 * applied.
 */
abstract class Formula {

    protected $myConnective;
    protected $myFormulae;

    public function __construct()
    {
        $this->myFormulae = array();
    }

    public function getFormulae()
    {
        return $this->myFormulae;
    }

    public function getArity()
    {
        return count($this->myFormulae);
    }

    public function getConnective()
    {
        return $this->myConnective;
    }

    public function equals(Formula $f)
    {
        return $this->toString() === $f->toString();
    }

    public function getUSubMap(Formula $subbedFormula)
    {
        $myMap = $this->USubbableConstrained($subbedFormula, $myMap);
    }

    abstract public function uSub(Formula $f, Atom $a);
    abstract public function uSubbable(Formula $uSub);
    abstract public function uSubbableConstrained(Formula $otherFormula, &$array);


}

?>