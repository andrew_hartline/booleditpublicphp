<?php

/**
 * The Simplification is a strange concept. It's basically a Proof without a clear
 * exit condition. You stop when the Formula is sufficiently "simple", but I don't
 * have an algorithm to determine when that is the case. Basically it should look
 * roughly like what you get from a K-map, and then you're done. I include it here
 * simply because it is an assigned topic in COMP 1113.
 */
class Simplification extends LinkedLines
{
    private $myFormula;

    public function __construct($f)
    {
        parent::__construct();
        $this->myFormula = mew Formula($f);
    }

    public function deleteLine($lineNo)
    {
        //echo "deleteLine<br>";
        $output = false;

        if (!is_int($lineNo))
            throw new IllegalArgumentException("deleteLine needs an int");

        $lineNo--; // Simplification is presented to user as beginning with line 1

        if (array_key_exists($lineNo, $this->myLines))
        {
           // echo "in proof.php, deleting line $lineNo";
            unset($this->myLines[$lineNo]);
            $output = true;
        }

        return $output;
    }

    // NOTE: call this after calling deleteLine() once the chips have settled (ie
    // after you have deleted all lines you want to delete). PHP does not reset
    // keys automatically so you have to do it yourseif: removing element at key
    // 1 in an array with keys 0, 1, 2, 3 will leave the array with 0, 2, 3
    public function resetLineNumbering()
    {
        $this->myLines = array_values($this->myLines);
    }

   public function addLine($formulaStr, $ruleStr)
    {
        //echo "passing $formulaStr to formulafactory\n";
        //echo "its tupe: " . gettype($formulaStr);
        $formula = BoolParser::formulaFactory($formulaStr);
        $rule = self::$rules->getRule($ruleStr);
        $line = new ProofLine($formula, $rule);
        //echo $line->toString();
        $this->myLines[] = $line;
    }

    public function replaceLine($i, ProofLine $p)
    {
        if ($i < 0)
        $inserted = array($p);
        array_splice( $this->myLines, $i, 0, $inserted );
        // inserting into a php array at an index is pretty weird
        // apparently this will order the keys correctly, I hope it does
        // TODO make sure this doesnt fuck up ordering
    }

/**
 * This method allows the Proof to evaluate each of its lines for OK-ness
 * according to its Rules.
 *
 * The first line is OK iff it is an instance of either side of the Proof's
 * Equivalence.
 *
 * Every line after that is OK iff it and its immediate predecessor are a
 * substitution instance of the pair of Formulae specified in the
 * EquivalenceRule cited at that line.
 * @return boolean True iff every line is ok (includes situation where there
 *                 aren't any lines).
 */
    public function evaluateLines()
    {
        foreach ($this->myLines as $lineNum => $line)
        {
            $f = $line->getFormula();
            $fstr = $f->toString();
            if ($lineNum == 0)
            {
               // echo "considering $lineNum $fstr<br>";
                if ($f->equals($this->myFormula)
                {
                    $line->setOk(true);
                    //echo "win for 1st line";
                }
                else
                    $line->setOk(false);
            }
            else
            {
                $rule = $line->getRule();
                $previousLine = $this->myLines[$lineNum-1]->getFormula();
                if ($rule != null)
                    $ok = $rule->licensedTransitionPart($previousLine,
                                                        $f);
                else $ok = false;
                $line->setOk($ok);
                if (!$ok)
                    $allGood = false;
               // else echo "another winner in line $lineNum";
            }
        }

        return $allGood;
    }


/**
 * Makes the simplification into XML. We can use this to save t he Proof to a db.
 * @return string xml
 */
    public function serialize()
    {
        $myXML = "<simplification>";
        $myXML .= "<formula>" . $this->myFormula->toString() . "</formula>";
        $myXML .= "<lines>";

        foreach ($this->myLines as $line)
        {
            $formula = $line->getFormula()->toString();
            $rule = ($line->getRule() == null)? "null" : $line->getRule()->getName();
            $ok = ($line->isOk())? "ok": "x";
            $myXML .= "<line><formula>$formula</formula>";
            $myXML .= "<rule>$rule</rule><ok>$ok</ok></line>";
        }

        $myXML .= "</lines>";
        $myXML .= "</proof>";

        return $myXML;
    }

    public static function unSerialize($string)
    {
        //echo $string . "\n";
        $x = simplexml_load_string($string);
        $array = explode("=", $x->equivalence);
        $s = new Simplification($array[0], $array[1]);

        foreach($x->lines->line as $line)
        {
            //echo $line->formula . " " . $line->rule . "\n";
            $s->addLine((string)$line->formula, (string)$line->rule);
        }
        //echo "end unSerialize\n\n";
        return $s;
    }

    public function getLines()
    {
        return $myLines;
    }

    public function getNumLines()
    {
        return count($this->myLines);
    }

    public function getRulesStringArray()
    {
        return self::$rules->toStringArray();
    }

}


}