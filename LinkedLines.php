<?php

require_once("includes.php");

/**
 * This is an abstract parent class for both Proofs and Simplifications. They
 * have different goals but both consist in successive lines linked together
 * by rules; this class captures that commonality.
 */
abstract class LinkedLines
{
    protected $myLines;

    public function __construct()
    {
        $this->myLines = array();
    }

    public function deleteLine($lineNo)
    {
        $output = false;
        $lineNo--; // Proof is presented to user as beginning with line 1

        if (array_key_exists($lineNo, $this->myLines))
        {
            unset($this->myLines[$lineNo]);
            $output = true;
        }

        return $output;
    }

    // NOTE: call this after calling deleteLine() once the chips have settled (ie
    // after you have deleted all lines you want to delete). PHP does not reset
    // keys automatically so you have to do it yourself: removing element at key
    // 1 in an array with keys 0, 1, 2, 3 will leave the array with keys 0, 2, 3
    public function resetLineNumbering()
    {
        $this->myLines = array_values($this->myLines);
    }

        public function addLine($formulaStr, $ruleStr)
    {
        $formula = BoolParser::formulaFactory($formulaStr);
        $rule = BooleRules::getRule($ruleStr);
        $line = new ProofLine($formula, $rule);
        $this->myLines[] = $line;
    }

    public function replaceLine($i, ProofLine $p)
    {
        if ($i < 0)
        $inserted = array($p);
        array_splice( $this->myLines, $i, 0, $inserted );
    }

    public function getLines()
    {
        return $this->myLines;
    }

    public function getNumLines()
    {
        return count($this->myLines);
    }

    public function getRulesStringArray()
    {
        return BooleRules::toStringArray();
    }
}
