<?php

require_once("includes.php");

/**
 * A Conjunction is a BinaryFormula whose connective is *, read as logical 'and'.
 */
class Conjunction extends BinaryFormula
{
    public function __construct(Formula $l, Formula $r)
    {
        parent::__construct($l, $r);
        $this->myConnective = "*";
    }
}

?>