<?php

require_once("includes.php");

class Negation extends UnaryFormula
{
    public function __construct(Formula $f)
    {
        parent::__construct($f);
        $this->myConnective = "'";
    }
}

?>