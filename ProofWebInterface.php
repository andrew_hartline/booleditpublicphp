<?php

/**
 * A basic little driver you can use to run BoolEdit.
 */
include("includes.php");

session_start();

if (isset($_SESSION['proof']))
{
    $p = Proof::unserialize($_SESSION['proof']);
    $serializedProof = $_SESSION['proof'];

// to the user, proofs begin at line 1, to us they begin at 0
    $numLinesUserSide = $p->GetNumLines() + 1;

    // This is a slightly dangerous section since we are deleting from the list
    // we are looping over. It helps us a lot that PHP is weird and doesn't
    // reindex when we delete stuff.
    for ($i = 1; $i < $numLinesUserSide; $i++)
    {
        if (isset($_POST["delete$i"]) && $_POST["delete$i"]=='on')
        {
            $p->deleteLine($i);
           // echo "tryed to delete line $i</br>";
        }
    }

    $p->resetLineNumbering(); // we save this for now and so safely delete all
                              // those lines
    $serializedProof = $p->serialize();
    $_SESSION['proof'] = $serializedProof;


    if ( isset($_POST['formula']) && !ctype_space($_POST['formula']) &&
         strlen($_POST['formula']) > 0 && isset($_POST['rule']) )
    {
        $formula = $_POST['formula'];
        $rule = $_POST['rule'];
        try
        {
            $p->addLine($formula, $rule);
            $p->evaluateLines();
            $serializedProof = $p->serialize();
            $_SESSION['proof'] = $serializedProof;
        }
        catch (SyntaxException $e)
        {
            echo "bad syntax in '$formula'<br>";
        }
    }

    displayProof($serializedProof);
    if ($p->getNumLines() == 0)
        promptForFirstLine($p);
    else
        promptForNewLine($p);
}
else if (isset($_POST['equivalence']))
{
    $array = explode("=", strip_tags($_POST['equivalence']));

    if (count($array) != 2)
    {
        echo "equivalence should be in the format A=B<br>";
        promptForEquivalence();
    }
    else
    {
        try
        {
            $p = new Proof($array[0], $array[1]);
            $serializedProof = $p->serialize();
            echo $serializedProof;
            $_SESSION['proof'] = $serializedProof;
            displayProof($serializedProof);
            promptForNewLine($p);
        }
        catch (SyntaxException $e)
        {
            echo "bad syntax<br>";
            promptForEquivalence();
        }
    }

}
else
{
    promptForEquivalence();
}

function promptForNewLine(Proof $p)
{
    echo "<input type='text' name = 'formula'/>Enter new line<br>";
    //echo "<input type='text' name = 'rule'/>Enter rule<br>";
    //
    echo "<select name='rule'>\n";
    $a = $p->GetRulesStringArray();
    foreach ($a as $ruleString)
    {
        // each rulestring is in the form 'rulename leftSide=rightSide'
        // we just want the rulename
        $ruleName = explode(" ", $ruleString)[0];
        echo "<option value='$ruleName'>$ruleString</option><br>\n";
    }

    echo "</select> Enter rule<br>";
    echo "<input type='submit'/></form>";
}

function promptForEquivalence()
{
    echo "<form method = 'POST' action = 'ProofWebInterface.php'>";
    echo "<input type='text' name = 'equivalence'/>Enter equivalence<br>";
    echo "<input type='submit'/><br></form>";
}

function promptForFirstLine(Proof $p)
{
    echo "<input type='text' name = 'formula'/>Enter first line<br>";
    //echo "<input type='text' name = 'rule'/>Enter rule<br>";
    echo "<input type = 'hidden' name='rule' value = 'null'/>";
    echo "<input type='submit'/></form>";
}

function displayProof($serializedProof)
{
    $x = simplexml_load_string($serializedProof);

    echo "<h3>Proof of " . $x->equivalence . " (" . $x->solved . ")</h3>\n";
    $lineNum = 1;

    echo "<form method = 'POST' action = 'ProofWebInterface.php'>\n";

    echo "<table>";

    foreach($x->lines->line as $line)
    {
        //echo $line->formula . " " . $line->rule . "\n";
        $formula = $line->formula;
        $rule = $line->rule;
        $ok = $line->ok;
        echo "<tr><td>$lineNum)</td><td>$formula</td><td>$rule</td><td>$ok<td>";
        echo "<td><input type='checkbox' name='delete$lineNum'/>delete<br></td>";
        $lineNum++;
    }

    echo "</table>";

    return;
}

?>