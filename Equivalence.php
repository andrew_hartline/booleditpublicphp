<?php

require_once("includes.php");

/**
 * Equivalences belong to Proofs; the class exists mostly for the
 * convenience of the equals() method. Informally an Equivalence
 * is a statement that two Formulae are interderivable or semantically
 * equivalent. Equivalences need not be true.
 */
class Equivalence
{
    private $left;
    private $right;

    public function __construct($l, $r)
    {
        $this->left = BoolParser::formulaFactory($l); // Formula class will take care of validation
        $this->right = BoolParser::formulaFactory($r);
    }

    public function getLeft()
    {
        return $this->left;
    }

    public function getRight()
    {
        return $this->right;
    }

    public function equals(Equivalence $e)
    {
        return ( $this->left->equals($e->getLeft()) &&
                 $this->right->equals($e->getRight()) ) ||
               ( $this->right->equals($e->getLeft()) &&
                 $this->left->equals($e->getRight()) );
    }

    public function toString()
    {
        return $this->left->toString() . " = " . $this->right->toString();

    }
}

?>