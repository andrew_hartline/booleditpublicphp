# README #

### What is this repository for? ###

This repository contains the back-end code for BoolEdit, a PHP program that evaluates proofs in Boolean algebra line by line. It is mostly intended to let people examine the source code, but can also be used as a working demo.

### How do I get set up? ###

The BoolEdit code base supports a variety of UIs. This instance contains a very bare-bones Web interface that can be used to check out how the program works. To try it out, you can host this repo with LAMP or your Apache server of choice, then point a browser at its root folder.

### What do I do now? ###

####Syntax
BoolEdit uses a basic grammar of Boolean algebra, as follows.

1a. An atom (in this case any Roman letter) is a formula.
1b. A constant (1 or 0) is a formula.
2. If A is a formula, A' is a formula.
3. If A and B are formulae, A + B is a formula.
4. If A and B are formulae, A * B is a formula.
5. Anything that is not a formula in virtue of 1-4 is not a formula. 

BoolEdit uses a standard order of operations (', *, then +) that can be defeated using parentheses.

####Writing a proof
When you are prompted to enter an equivalence, the program expects to see two syntactically valid Boolean algebraic formulae with an equals sign in the middle. You could try p + q = (p + q)'' to start.

The first line of the proof can be any syntactically valid Boolean algebraic formula, but only the left or right hand side of the equivalence will evaluate as correct. If you are proving p + q = (p + q)'', you could start with p + q.

Thereafter, you can extend the proof however you choose, but only correct uses of Boolean algebraic rules will evaluate as correct. If you are working on p + q = (p + q)'', you could enter (p + q)'' on line 2 using T13a, which permits double negation to be added or removed to arbitrary subformulae of any formula. 

Note that BoolEdit is not as permissive as a human being when it comes to marking your proofs. You need to take care to apply only one rule at a time. For example, a human might think it is fine to use T13a to justify the addition or removal of any even number of negations, but BoolEdit will only let you do two at a time.


### What is interesting about this code? ###

BoolEdit is based on the Formula object. It is a GoF Composite in that a Formula can contain recursive aggregations of Formulae. This is a natural fit since Boolean algebraic formulae are structured in precisely this way.

BoolEdit incorporates a somewhat complex class hierarchy. At any rate it proved to be too deep for Javascript, which supports only a single level of inheritance. I wrestled with the thought of flattening out my hierarchy, but all of its levels find a use in the algorithm for determining correct use of rules.

The program has a complete parser for Boolean algebra whose role is to take user strings and make them into Formula objects. This parser is based on Dijkstra's Shunting Yard Algorithm. It forms the core functionality of a GoF Factory. This factory is absolutely necessary because Formula is an abstract class and so cannot have a constructor.

BoolEdit's rule checker makes use of a parallel tree traversal algorithm. 

The basic version of this algorithm takes two Formula objects and checks whether they form a substitution instance of the pair of formulae that form the rule that has been cited. Thus the rule T13a (p = p'') permits the interderivation of P + Q and (P + Q)''. 

The more complicated version of this algorithm searches every node of two Formula objects to determine whether exactly one of their parallel subformulae represents a valid use of the rule cited as in the basic version, above. Thus the rule T13a (p = p'') permits the interderivation of P + Q and P'' + Q. 

Enjoy, and please feel free to email me with questions or comments at andrewhartline@gmail.com.