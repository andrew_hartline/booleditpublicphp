<?php

require_once("Formula.php");

abstract class UnaryFormula extends CompositeFormula implements SimplePrint
{
    public function __construct(Formula $f)
    {
        if ($f instanceof Formula)
            $this->myFormulae = array($f);
        else throw new SyntaxException("I need fully constructed Formulae");
    }

    public function toString()
    {
        if ($this->myFormulae[0] instanceof SimplePrint)
            $output = $this->myFormulae[0]->toString() .
                      $this->myConnective;
        else
            $output = "(" .
                      $this->myFormulae[0]->toString() .
                      ")" .
                      $this->myConnective;

        return $output;
    }
}